package com.mateoscorp.miedadcaninakotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.mateoscorp.miedadcaninakotlin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCalcular.setOnClickListener {
            val et_ingresa: String = binding.etIngresaEdad.text.toString()
            if (et_ingresa.isNotEmpty()){
                var numIngresa: Int = et_ingresa.toInt()
                var resultado = numIngresa * 7
                var resultadoString: String = getString(R.string.resultado,resultado)
                binding.tvResultado.text = resultadoString
            }else{
                Toast.makeText(this, R.string.field_empty, Toast.LENGTH_SHORT).show()
            }
        }
        binding.btnLimpiar.setOnClickListener{
            var limpieza:String = ""
            binding.tvResultado.text = limpieza
            binding.etIngresaEdad.setText(limpieza)
            Toast.makeText(this, getString(R.string.clean_field), Toast.LENGTH_SHORT).show()
        }
    }
}